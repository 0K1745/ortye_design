// Step 1: Import React
import * as React from 'react'
import Layout from '../components/layout'
import {StaticImage} from "gatsby-plugin-image";
import {Link} from 'gatsby';
// Step 2: Define your component
const IndexPage = () => {
    return (
        <Layout pageTitle="Ortye design">
            <div className="tile is-ancestor">
                <article className="tile is-child box">
                    <figure className="image is-3by5">
                        <img src="https://gitlab.com/0K1745/ortye_design/-/raw/main/src/images/ortye_.png" alt={"ortye profile picture"}/>
                    </figure>
                </article>
                <article className="tile is-child box">

                    <Link to="https://www.instagram.com/ortye_/">
                        <figure className="image is-3by5 ">
                            <img src="https://gitlab.com/0K1745/ortye_design/-/raw/main/src/images/Instagram_logo_2016.svg" alt={"instagram logo"}/>
                        </figure>
                    </Link>
                </article>
                <article className="tile is-child box">

                    <Link to="https://www.instagram.com/ortye_/">
                        <figure className="image is-3by5">
                            <img src="https://gitlab.com/0K1745/ortye_design/-/raw/main/src/images/Etsy_logo.svg" alt={"instagram logo"}/>
                        </figure>
                    </Link>
                </article>
            </div>
        </Layout>

    )
}

// Step 3: Export your component
export default IndexPage