// Step 1: Import React
import * as React from 'react'
import Layout from '../components/layout'
import {StaticImage} from "gatsby-plugin-image";
// Step 2: Define your component
const AboutPage = () => {
    return (
        <Layout pageTitle="Morgane Cholet">


            <div className={"content"}>
                <div className={"columns"}>
                    <div className={"column is-one-third"}>
                        <StaticImage width={475/2} height={425/2} src={"../images/ortye_.png"} alt={"ortye profile picture"}/>
                    </div>
                    <div className={"column"}>

                        <p>
                            Etudiante au lycée professionnel d’Arsonval en terminale communication visuelle. Agée de
                            18
                            ans, je
                            suis une jeune fille souriante, créative. Pour toute demande ,logo, affiche, bulletin
                            d'accueil,
                            flyer, illustration... je serais heureuse de faire équipe avec vous afin de réaliser
                            votre
                            projet.
                        </p>
                    </div>
                </div>
                <h1>PARCOURS PROFESSIONNEL</h1>

                <div className={"columns"}>
                    <div className={"column is-one-third"}>
                        BAC PROFESSIONELLE
                    </div>
                    <div className={"column"}>

                        Lycée professionnel d’Arsonval
                        en communication visuelle
                        à Joué-lès-Tours
                    </div>
                </div>
                <div className={"columns "}>

                    <div className={"column is-one-third"}>
                        EXPÉRIENCE PROFESSIONNELLE
                    </div>
                    <ul>
                        <li>
                            2021 : Stage de terminale agence de communication Ignis Saumur (49)
                        </li>
                        <li>
                            2020 : Stage de terminale Communauté de Communes Bourgueil (37)
                        </li>
                        <li>
                            2019 : Stage de première à la Mairie de La Turballe (44)
                        </li>
                        <li>
                            2019 : Stage de seconde à la Mairie de Bourgueil (37)
                        </li>
                        <li>
                            2019 : Stage de seconde à l’Imprimerie Gambetta à Château La Vallière (37)
                        </li>
                        <li>
                            2018 : Stage de 3eme à La Chambre Noir de Bourgueil (37)
                        </li>
                    </ul>
                </div>
            </div>

        </Layout>
    )
}

// Step 3: Export your component
export default AboutPage