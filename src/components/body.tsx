import * as React from 'react'

const Body = ({ pageTitle, children }) => {
   return (
        <div className="container">

            <div className="is-full pl-5 above-footer">
                <main >
                    <h1 className="title" >{pageTitle}</h1>
                    {children}
                </main>
            </div>
        </div>
    )
}

export default Body