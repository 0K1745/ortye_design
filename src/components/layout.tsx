import * as React from 'react'
import Footer from './footer'
import Header from './header'
import Body from './body'
import "../pages/mystyles.scss"

const Layout = ({ pageTitle, children }) => {
    return (
        <div className="container">
            <Header pageTitle={pageTitle}></Header>
            <Body pageTitle={pageTitle}>{children}</Body>
            <Footer></Footer>
        </div>
    )
}

export default Layout