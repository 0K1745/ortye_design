import * as React from 'react'
import {Link, useStaticQuery, graphql} from 'gatsby'
import {StaticImage} from "gatsby-plugin-image";

const Header = ({pageTitle}) => {
    const data = useStaticQuery(graphql`
    query{
        site {
          siteMetadata {
            title
          }
        }
      }
  `)


    return (
        <div className="container">
            <div className="is-full">
                <nav>
                    <ul className="navbar-item  navbar-end">
                        <Link to="/">
                            <StaticImage width={51} height={37} src={"../images/icon.png"} alt={"ortye logo"}/>
                        </Link>
                        <li>
                            <Link to="/">
                                Home
                            </Link>
                            <span className={"mx-2"}></span>
                            <Link to="/about">
                                About
                            </Link>
                        </li>
                        <li className="navbar-item navbar-end">
                            <h1><strong>Ortye_</strong></h1>
                        </li>
                        <span className={"mr-5"}></span>
                        <li className="navbar-item navbar-end">
                            <Link to="https://www.instagram.com/ortye_/">
                                <StaticImage src={"../images/instagram.svg"} alt={"instagram logo"}/>
                            </Link>
                            <span className={"mx-2"}></span>
                            <Link to="mailto://saunier.steph@gmail.com">
                                <StaticImage src={"../images/mail.png"} alt={"instagram logo"}/>
                            </Link>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    )
}

export default Header