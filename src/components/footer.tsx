import * as React from 'react'
import {StaticImage} from "gatsby-plugin-image";

const Footer = () => {
    return (
        <div className="container">
            <div className="footer">
                <div className="content has-text-centered">
                    <StaticImage width={51} height={37} src={"../images/icon.png"} alt={"ortye logo"}/>
                        <strong>Ortye design & creations</strong>.
                </div>
            </div>
        </div>
    )
}

export default Footer